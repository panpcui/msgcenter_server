package msgcenter.controller;

import java.util.List;

import msgcenter.model.Group;
import msgcenter.service.GroupServiceI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody; 

@Controller
@RequestMapping("/groupController")
public class GroupController {

	private GroupServiceI groupService;

	public GroupServiceI getGroupService() {
		return groupService;
	}

	@Autowired
	public void setGroupService(GroupServiceI groupService) {
		this.groupService = groupService;
	}

	@RequestMapping("/showAllGroup")
	public @ResponseBody List<Group> showAllGroup() {
		return groupService.showAllGroup();

	}
	
	@RequestMapping("/showGroupByUserId")
	public @ResponseBody List<Group> showGroupByUserId(long id) {
		return groupService.showGroupByUserId(id);

	}
}
