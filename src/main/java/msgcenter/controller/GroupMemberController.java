package msgcenter.controller;

import java.util.List;

import msgcenter.model.GroupMember;
import msgcenter.service.GroupMemberServiceI;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/groupMemberController")
public class GroupMemberController {


	private GroupMemberServiceI groupMemberService;

	public GroupMemberServiceI getGroupService() {
		return groupMemberService;
	}
	@Autowired
	public void setGroupService(GroupMemberServiceI groupMemberService) {
		this.groupMemberService = groupMemberService;
	}
	
	@RequestMapping("/showGroupMemberByGroupId")
	public @ResponseBody List<GroupMember> showGroupMemberByGroupId(long id)
	{
		return groupMemberService.showGroupMemberByGroupId(id);
	}
}
