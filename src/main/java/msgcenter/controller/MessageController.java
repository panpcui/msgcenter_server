package msgcenter.controller;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import msgcenter.model.Message;
import msgcenter.service.MessageServiceI;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/messageController")
public class MessageController {
	private MessageServiceI messageService;

	public MessageServiceI getMessageService() {
		return messageService;
	}
	
	@Autowired
	public void setMessageService(MessageServiceI messageService) {
		this.messageService = messageService;
	}
	
	@RequestMapping("/showAllMessage")
	public @ResponseBody Map<String,Object> showAllMessage(Integer page, Integer rows) {
		return messageService.showAllMessage(page, rows);
	}

	@RequestMapping("/showMessageByUsername")
	public @ResponseBody Map<String,Object> showMessageByUsername(String username, Integer page, Integer rows) {
		return messageService.showMessageByUsername(username, page, rows);
	}
	
	@RequestMapping("/showMessageByQureyItems")
	public @ResponseBody Map<String,Object> showMessageByQureyItems(String qureyString, String dateString, Integer page, Integer rows) {
		return messageService.showMessageByQureyItems(qureyString, dateString, page, rows);
	}
	
	@RequestMapping("/pushMessage")
	public @ResponseBody Map<String,Object> pushMsg(String message,String pushTag,String deviceType, HttpSession session) {
		return messageService.pushMsg(message, pushTag, deviceType, session);
	}
	
	@RequestMapping("/deleteMessage")
	public @ResponseBody Map<String,Object> deleteMessage(String id) {
		return messageService.deleteMessagebyId(id);
	}
	
	
}
