package msgcenter.controller;

import java.io.File;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import msgcenter.model.News;
import msgcenter.service.NewsServiceI;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

@Controller
@RequestMapping("/newsController")
public class NewsController {
	private NewsServiceI newsService;

	public NewsServiceI getNewsService() {
		return newsService;
	}

	@Autowired
	public void setNewsService(NewsServiceI newsService) {
		this.newsService = newsService;
	}

	@RequestMapping("/showAllNews")
	public @ResponseBody List<News> getNews(){
		return newsService.selectAllNews();
	}

	@RequestMapping("/deleteNews")
	public @ResponseBody void deleteNews(Long id){
		newsService.deleteByPrimaryKey(id);
	}

	@RequestMapping(value = "/upload")
	public @ResponseBody int insertNews(@RequestParam(value = "file", required = false) MultipartFile[] files, HttpServletRequest request, ModelMap model){
		String imgpath=new String();
		StringBuffer fileUrl=new StringBuffer();
		String title=request.getParameter("title");
		System.out.print(title);
		String content=request.getParameter("content");
		String tag=request.getParameter("tag");
		
		
		System.out.println("开始");  
        String imgPath = request.getSession().getServletContext().getRealPath("imgFile"); 
        String filePath = request.getSession().getServletContext().getRealPath("file"); 
        File targetFile=null;
        for(MultipartFile file:files){
	        String fileName = file.getOriginalFilename(); 
	        if(fileName.contains(".jpg")||fileName.contains(".png")||fileName.contains(".bmp")||fileName.contains(".jpeg")){       //图片处理
		       fileName = new Date().getTime()+".jpg";  
		        System.out.println(imgpath);  
		         targetFile= new File(imgPath, fileName);  
		        if(!targetFile.exists()){  
		            targetFile.mkdirs();  
		        }  
	        }else{
	        	fileName = new Date().getTime()+fileName;                //普通附件处理
		        System.out.println(filePath);  
		         targetFile= new File(filePath, fileName);  
		        if(!targetFile.exists()){  
		            targetFile.mkdirs();  
		            if(!fileUrl.toString().equals("")){
		            	fileUrl.append(",");
		            }
		            fileUrl.append(filePath+fileName);
		        }  
	        }
	        //保存  
	        try {  
	            file.transferTo(targetFile);  
	        } catch (Exception e) {  
	            e.printStackTrace();  
	        }  
	        model.addAttribute("fileUrl", request.getContextPath()+"/upload/"+fileName);  	  
        }
        
		News news=new News();
		news.setContent(content);
		news.setImgUrl(imgpath);
		news.setFileUrl(fileUrl.toString());
		news.setTitle(title);
        news.setPublisherId(Long.parseLong("100000"));
        news.setCreateTime(new Date());
        news.setAlterTime(new Date());
		newsService.insert(news);
		return 1;
	}

}
