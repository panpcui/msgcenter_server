package msgcenter.controller;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import msgcenter.model.User;
import msgcenter.service.GroupMemberServiceI;
import msgcenter.service.UserServiceI;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

@Controller
@RequestMapping("/userController")
@SessionAttributes("username")
public class UserController {

	private UserServiceI userService;
	private GroupMemberServiceI groupMemberService;

	public GroupMemberServiceI getGroupMemberService() {
		return groupMemberService;
	}

	@Autowired
	public void setGroupMemberService(GroupMemberServiceI groupMemberService) {
		this.groupMemberService = groupMemberService;
	}

	public UserServiceI getUserService() {
		return userService;
	}

	@Autowired
	public void setUserService(UserServiceI userService) {
		this.userService = userService;
	}

	@RequestMapping("/userInfo")
	public @ResponseBody
	User getUser(@RequestParam Long id) {
		User user = userService.getUserById(id);
		return user;
	}

	@RequestMapping("/getSessionUsername")
	public @ResponseBody
	String getSessionUsername(HttpSession session) {
		return (String)session.getAttribute("username");
	}
	
	@RequestMapping("/clearSessionUsername")
	public @ResponseBody
	void clearSessionUsername(SessionStatus sessionStatus, HttpSession session) {
		sessionStatus.setComplete();
		session.removeAttribute("username");
	}
	
	/**
	 * 检查登录接口
	 * 
	 * @param username
	 *            用户名
	 * @param password
	 *            密码
	 * @return result:true or false
	 * 
	 */
	@RequestMapping("/checkUserLogin")
	public @ResponseBody
	Map<String, Object> checkUserLogin(String username, String password,
			Long channelid, String userid, Model model, SessionStatus sessionStatus, HttpSession session) {
		return userService.checkUserLogin(username, password, channelid, userid, model, sessionStatus, session);
	}

	@RequestMapping("/showAllUser")
	public @ResponseBody
	List<User> showAllUser() {
		return userService.showAllUser();
	}
}
