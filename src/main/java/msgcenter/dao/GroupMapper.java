package msgcenter.dao;

import java.util.List;

import msgcenter.model.Group;

public interface GroupMapper {
    int deleteByPrimaryKey(Long id);

    int insert(Group record);

    int insertSelective(Group record);

    Group selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(Group record);

    int updateByPrimaryKey(Group record);

	List<Group> showAllGroup();

	List<Group> showGroupByUserId(long id);
}