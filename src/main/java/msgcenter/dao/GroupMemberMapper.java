package msgcenter.dao;

import java.util.List;

import msgcenter.model.GroupMember;

public interface GroupMemberMapper {

    int insert(GroupMember record);

    int insertSelective(GroupMember record);

    int updateByPrimaryKeySelective(GroupMember record);

    int updateByPrimaryKey(GroupMember record);
    
    List<Long> getGroupIdByUserId(Long userId);

	List<GroupMember> showGroupMemberByGroupId(long id);
	
}