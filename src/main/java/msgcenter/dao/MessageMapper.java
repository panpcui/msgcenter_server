package msgcenter.dao;

import java.util.List;
import java.util.Map;

import msgcenter.model.Message;

public interface MessageMapper {
    int deleteByPrimaryKey(Long id);

    int insert(Message record);

    int insertSelective(Message record);

    Message selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(Message record);

    int updateByPrimaryKey(Message record);

    int getAllMessageCount();
    
	List<Message> showAllMessage(Map<String, Object> map);

	int getMessageByUsernameCount(String username);
	
	List<Message> showMessageByUsername(Map<String, Object> map);
	
	int getMessageByQureyItemsCount(Map<String, Object> map);

	List<Message> showMessageByQureyItems(Map<String, Object> map);

}