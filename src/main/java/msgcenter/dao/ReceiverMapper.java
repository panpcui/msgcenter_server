package msgcenter.dao;

import msgcenter.model.Receiver;

public interface ReceiverMapper {

    int insert(Receiver record);

    int insertSelective(Receiver record);

    int updateByPrimaryKeySelective(Receiver record);

    int updateByPrimaryKey(Receiver record);
    
}