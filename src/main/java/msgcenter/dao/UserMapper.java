package msgcenter.dao;

import java.util.HashMap;
import java.util.List;

import msgcenter.model.User;

public interface UserMapper {
    int deleteByPrimaryKey(Long id);

    int insert(User record);

    int insertSelective(User record);

    User selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(User record);

    int updateByPrimaryKey(User record);
    
    User checkUserLogin(HashMap<String, String> map);
    
    int updateUserInfoByUsername(HashMap<String, Object> map);
    
    List<User> showAllUser();
    
    List<Long> getUserIdByType(int type);
}