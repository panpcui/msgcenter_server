package msgcenter.model;

public class Group {
    private Long id;

    private Long belongerId;

    private String name;

    private Integer tag;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getBelongerId() {
        return belongerId;
    }

    public void setBelongerId(Long belongerId) {
        this.belongerId = belongerId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public Integer getTag() {
        return tag;
    }

    public void setTag(Integer tag) {
        this.tag = tag;
    }
}