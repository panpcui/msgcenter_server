package msgcenter.model;

import java.util.Date;

import msgcenter.util.CustomDateSerializer;

import org.codehaus.jackson.map.annotate.JsonSerialize;

public class Log {
    private Long id;

    private String operation;

    private Long userId;

    private Date operationTime;

    private Integer tag;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getOperation() {
        return operation;
    }

    public void setOperation(String operation) {
        this.operation = operation == null ? null : operation.trim();
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    @JsonSerialize(using = CustomDateSerializer.class)
    public Date getOperationTime() {
        return operationTime;
    }

    public void setOperationTime(Date operationTime) {
        this.operationTime = operationTime;
    }

    public Integer getTag() {
        return tag;
    }

    public void setTag(Integer tag) {
        this.tag = tag;
    }
}