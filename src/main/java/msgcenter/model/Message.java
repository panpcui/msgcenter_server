package msgcenter.model;

import java.util.Date;

import msgcenter.util.CustomDateSerializer;

import org.codehaus.jackson.map.annotate.JsonSerialize;

public class Message {
    private Long id;

    private String content;

    private Long senderId;
    
    private String senderName;

    private Date sendTime;

    private Integer tag;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content == null ? null : content.trim();
    }

    public Long getSenderId() {
        return senderId;
    }

    public void setSenderId(Long senderId) {
        this.senderId = senderId;
    }

    public String getSenderName() {
		return senderName;
	}

	public void setSenderName(String senderName) {
		this.senderName = senderName;
	}

	@JsonSerialize(using = CustomDateSerializer.class)
	public Date getSendTime() {
        return sendTime;
    }

    public void setSendTime(Date sendTime) {
        this.sendTime = sendTime;
    }

    public Integer getTag() {
        return tag;
    }

    public void setTag(Integer tag) {
        this.tag = tag;
    }
}