package msgcenter.model;

import java.util.Date;

import msgcenter.util.CustomDateSerializer;

import org.codehaus.jackson.map.annotate.JsonSerialize;

public class News {
    private Long id;

    private String title;

    private String content;

    private Integer type;

    private Integer permission;

    private Long publisherId;

    private String publisherName;
    
    private String imgUrl;

    private String fileUrl;

    private Date createTime;

    private Date alterTime;

    private Integer tag;

    
    public News(){
    	
    }
    
    public News(Long id, String title, String content, Integer type,
			Integer permission, Long publisherId, String imgUrl,
			String fileUrl, Date createTime, Date alterTime, Integer tag) {
		this.id = id;
		this.title = title;
		this.content = content;
		this.type = type;
		this.permission = permission;
		this.publisherId = publisherId;
		this.imgUrl = imgUrl;
		this.fileUrl = fileUrl;
		this.createTime = createTime;
		this.alterTime = alterTime;
		this.tag = tag;
	}

	public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title == null ? null : title.trim();
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content == null ? null : content.trim();
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getPermission() {
        return permission;
    }

    public void setPermission(Integer permission) {
        this.permission = permission;
    }

    public Long getPublisherId() {
        return publisherId;
    }

    public void setPublisherId(Long publisherId) {
        this.publisherId = publisherId;
    }

    public String getPublisherName() {
		return publisherName;
	}

	public void setPublisherName(String publisherName) {
		this.publisherName = publisherName;
	}

	public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl == null ? null : imgUrl.trim();
    }

    public String getFileUrl() {
        return fileUrl;
    }

    public void setFileUrl(String fileUrl) {
        this.fileUrl = fileUrl == null ? null : fileUrl.trim();
    }

    @JsonSerialize(using = CustomDateSerializer.class)
    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    @JsonSerialize(using = CustomDateSerializer.class)
    public Date getAlterTime() {
        return alterTime;
    }

    public void setAlterTime(Date alterTime) {
        this.alterTime = alterTime;
    }

    public Integer getTag() {
        return tag;
    }

    public void setTag(Integer tag) {
        this.tag = tag;
    }
}