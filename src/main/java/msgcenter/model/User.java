package msgcenter.model;

import java.util.Date;

import msgcenter.util.CustomDateSerializer;

import org.codehaus.jackson.map.annotate.JsonSerialize;

public class User {
    private Long id;

    private String username;

    private String password;

    private String name;

    private Integer type;

    private String userTag;

    private Long channelTag;

	private Date loginTime;

    private Integer tag;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username == null ? null : username.trim();
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password == null ? null : password.trim();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getUserTag() {
        return userTag;
    }

    public void setUserTag(String userTag) {
        this.userTag = userTag == null ? null : userTag.trim();
    }

    public Long getChannelTag() {
		return channelTag;
	}

	public void setChannelTag(Long channelTag) {
		this.channelTag = channelTag;
	}
	
	@JsonSerialize(using = CustomDateSerializer.class)
    public Date getLoginTime() {
        return loginTime;
    }

    public void setLoginTime(Date loginTime) {
        this.loginTime = loginTime;
    }

    public Integer getTag() {
        return tag;
    }

    public void setTag(Integer tag) {
        this.tag = tag;
    }
}