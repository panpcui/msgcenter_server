package msgcenter.pushManager;

import javapns.back.PushNotificationManager;  
import javapns.back.SSLConnectionHelper;  
import javapns.data.Device;  
import javapns.data.PayLoad;  

public class APNSPushManager {
	 private static APNSPushManager mgr;
	 private int badge;	//图标小红圈的数值
	 private String sound;
	 PayLoad payLoad;
	//Connect to APNs  
    /**
     测试的服务器地址：gateway.sandbox.push.apple.com /端口2195  
产品推送服务器地址：gateway.push.apple.com / 2195  
    */
	 private String host;
	 int port;
	 private String certificatePath;
	 private String certificatePassword;
	 PushNotificationManager pushManager;
	 
	 public APNSPushManager() throws Exception{
		 sound="default";
		 badge=1;
		 payLoad=new PayLoad();
		 payLoad.addBadge(badge);//图标小红圈的数值 
		 payLoad.addSound(sound);//铃音
		 certificatePath="D:\\test\\mobile.p12";
		 certificatePassword="mobile";
		 host="gateway.sandbox.push.apple.com";
		 port=2195;
		 pushManager = PushNotificationManager.getInstance();
		 pushManager.initializeConnection(host,port, certificatePath,certificatePassword, SSLConnectionHelper.KEYSTORE_TYPE_PKCS12);
	 }
	 public static APNSPushManager getInstance() throws Exception{
		if (mgr == null) {
			mgr = new APNSPushManager();
		}
		return mgr;
	}
	/**
	 * 迎新结果推送方法
	 * @param deviceToken 设备号
	 * @param message 推送消息内容
	 * @param title 
	 * @return void
	 */
	 public void WelcomeResultPush(String deviceToken,String message){
		 try {
			payLoad.addCustomDictionary("title","报到结果");
			payLoad.addCustomDictionary("type", "0");//0表示迎新结果类型
			payLoad.addAlert(message);//push的内容
			pushManager.addDevice("iPhone", deviceToken);
			Device client = pushManager.getDevice("iPhone");
            pushManager.sendNotification(client, payLoad);
            pushManager.stopConnection();
            pushManager.removeDevice("iPhone");  
		}catch (Exception e) {
            e.printStackTrace();
           }
	 }
	 /**
	  * 新闻推送方法
	  * @param deviceTokens 设备号数组
	  * @param message 推送消息
	  * @return void
	  */
	 public void NewsPush(String[] deviceTokens,String message){
		 
	 }
	 
    public static void main(String[] args) throws Exception {  
    	String deviceToken = "ae38e153c8e5cd2019b9e6895b5047cef9263e165c2f151120fa060842920d1d";//iphone手机获取的token
    	String title="Test";
    	String message="this is for test";
    	mgr=APNSPushManager.getInstance();
    	mgr.WelcomeResultPush(deviceToken, message);
    } 
}
