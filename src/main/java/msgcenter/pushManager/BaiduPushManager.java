package msgcenter.pushManager;

import com.baidu.yun.channel.auth.ChannelKeyPair;
import com.baidu.yun.channel.client.BaiduChannelClient;
import com.baidu.yun.channel.exception.ChannelClientException;
import com.baidu.yun.channel.exception.ChannelServerException;
import com.baidu.yun.channel.model.PushTagMessageRequest;
import com.baidu.yun.channel.model.PushTagMessageResponse;
import com.baidu.yun.core.log.YunLogEvent;
import com.baidu.yun.core.log.YunLogHandler;

public class BaiduPushManager {
	private String apiKey = "hApKBT6ashrqoW3hz6UCIZkO";
	private String secretKey = "bNrOVCB0PGkbBPNrKcGXTGZV0Q7jPIjh";
	ChannelKeyPair pair = null;
	BaiduChannelClient channelClient = null;

	public BaiduPushManager() {
		pair = new ChannelKeyPair(apiKey, secretKey);
		channelClient = new BaiduChannelClient(pair);
		channelClient.setChannelLogHandler(new YunLogHandler() {
			public void onHandle(YunLogEvent event) {
				System.out.println(event.getMessage());
			}
		});
	}

	public boolean pushMessageByTag(String pushTag, String message) {
        try {

            // 4. 创建请求类对象
            PushTagMessageRequest request = new PushTagMessageRequest();
            request.setDeviceType(3); // device_type => 1: web 2: pc 3:android
                                      // 4:ios 5:wp
            request.setTagName(pushTag);
            request.setMessage(message);
            //request.setDeployStatus(1);
            // 若要通知，
            // request.setMessageType(1);
            // request.setMessage("{\"title\":\"Notify_title_danbo\",\"description\":\"Notify_description_content\"}");

            // 5. 调用pushMessage接口
            PushTagMessageResponse response = channelClient.pushTagMessage(request);

            // 6. 认证推送成功
            System.out.println("push amount : " + response.getSuccessAmount());

        } catch (ChannelClientException e) {
            // 处理客户端错误异常
            e.printStackTrace();
            return false;
        } catch (ChannelServerException e) {
            // 处理服务端错误异常
            System.out.println(String.format(
                    "request_id: %d, error_code: %d, error_message: %s",
                    e.getRequestId(), e.getErrorCode(), e.getErrorMsg()));
            return false;
        }
        return true;
	}
	public void pushMessageByUser(Long channelid,String userid,String message){
		
	}

	
	
	public static void main(String[] args) {
		BaiduPushManager bpm=new BaiduPushManager();
		bpm.pushMessageByTag("teacher", "你好老师");
	}
}
