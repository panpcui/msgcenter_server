package msgcenter.service;

import java.util.List;
import msgcenter.model.GroupMember;

public interface GroupMemberServiceI {
	public List<Long> getGroupIdByUserId(Long userId);

	public List<GroupMember> showGroupMemberByGroupId(long id);
}
