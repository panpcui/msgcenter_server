package msgcenter.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import msgcenter.dao.GroupMemberMapper;
import msgcenter.model.GroupMember;

@Service("groupMemberService")
public class GroupMemberServiceImpl implements GroupMemberServiceI {
	private GroupMemberMapper groupMemberMapper;

	public GroupMemberMapper getGroupMemberMapper() {
		return groupMemberMapper;
	}

	@Autowired
	public void setGroupMemberMapper(GroupMemberMapper groupMemberMapper) {
		this.groupMemberMapper = groupMemberMapper;
	}

	public List<Long> getGroupIdByUserId(Long userId) {
		// TODO Auto-generated method stub
		List<Long> list = groupMemberMapper.getGroupIdByUserId(userId);
		return list;
	}

	public List<GroupMember> showGroupMemberByGroupId(long id) {
		return groupMemberMapper.showGroupMemberByGroupId(id);
	}

}
