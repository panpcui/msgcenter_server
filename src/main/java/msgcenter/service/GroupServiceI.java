package msgcenter.service;

import java.util.List;

import msgcenter.model.Group;

public interface GroupServiceI {

	public Group getGroupById(Long id);
	public List<Group> showAllGroup();
	public List<Group> showGroupByUserId(long id);
}
