package msgcenter.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import msgcenter.dao.GroupMapper;
import msgcenter.model.Group;

@Service("groupService")
public class GroupServiceImpl implements GroupServiceI {

	private GroupMapper groupMapper;

	public GroupMapper getGroupMapper() {
		return groupMapper;
	}

	@Autowired
	public void setGroupMapper(GroupMapper groupMapper) {
		this.groupMapper = groupMapper;
	}

	public Group getGroupById(Long id) {
		// TODO Auto-generated method stub
		return groupMapper.selectByPrimaryKey(id);
	}

	public List<Group> showAllGroup() {
		return groupMapper.showAllGroup();
	}

	public List<Group> showGroupByUserId(long id) {
		return groupMapper.showGroupByUserId(id);
	}
}
