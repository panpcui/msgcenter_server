package msgcenter.service;

import java.util.Map;

import javax.servlet.http.HttpSession;

public interface MessageServiceI {
	
	public Map<String,Object> showAllMessage(Integer page, Integer rows);
	
	public Map<String, Object> deleteMessagebyId(String id);

	public Map<String,Object> showMessageByUsername(String username, Integer page, Integer rows);

	public Map<String,Object> showMessageByQureyItems(String qureyString,
			String dateString, Integer page, Integer rows);

	public Map<String, Object> pushMsg(String message, String pushTag,
			String deviceType, HttpSession session);
}
