package msgcenter.service;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import msgcenter.dao.MessageMapper;
import msgcenter.dao.ReceiverMapper;
import msgcenter.dao.UserMapper;
import msgcenter.model.Message;
import msgcenter.model.Receiver;
import msgcenter.pushManager.BaiduPushManager;

@Service("messageService")
public class MessageServiceImpl implements MessageServiceI {
	private MessageMapper messageMapper;
	private UserMapper userMapper;
	private ReceiverMapper receiverMapper;

	public MessageMapper getMessageMapper() {
		return messageMapper;
	}

	@Autowired
	public void setMessageMapper(MessageMapper messageMapper) {
		this.messageMapper = messageMapper;
	}

	public UserMapper getUserMapper() {
		return userMapper;
	}

	@Autowired
	public void setUserMapper(UserMapper userMapper) {
		this.userMapper = userMapper;
	}

	public ReceiverMapper getReceiverMapper() {
		return receiverMapper;
	}

	@Autowired
	public void setReceiverMapper(ReceiverMapper receiverMapper) {
		this.receiverMapper = receiverMapper;
	}

	public Map<String, Object> showAllMessage(Integer page, Integer rows) {
		if (page == null) {
			page = 1;
		}
		if (rows == null) {
			rows = 10;
		}
		Map<String, Object> jsonMap = new HashMap<String, Object>();

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("pageSize", rows);
		map.put("offSet", (page - 1) * rows);

		List<Message> messageList = messageMapper.showAllMessage(map);

		jsonMap.put("total", messageMapper.getAllMessageCount());
		jsonMap.put("rows", messageList);

		return jsonMap;
	}

	public Map<String, Object> deleteMessagebyId(String id) {
		Map<String, Object> jsonMap = new HashMap<String, Object>();
		boolean m_r = false;
		if (messageMapper.deleteByPrimaryKey(Long.parseLong(id)) == 1) {
			m_r = true;
			jsonMap.put("result", m_r);
		}
		return jsonMap;
	}

	public Map<String, Object> showMessageByUsername(String username, Integer page,
			Integer rows) {
		if (page == null) {
			page = 1;
		}
		if (rows == null) {
			rows = 10;
		}
		
		Map<String, Object> jsonMap = new HashMap<String, Object>();

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("username", username);
		map.put("pageSize", rows);
		map.put("offSet", (page - 1) * rows);

		List<Message> messageList = messageMapper.showMessageByUsername(map);

		jsonMap.put("total", messageMapper.getMessageByUsernameCount(username));
		jsonMap.put("rows", messageList);

		return jsonMap;
	}

	@Override
	public Map<String, Object> showMessageByQureyItems(String qureyString,
			String dateString, Integer page, Integer rows) {
		if (page == null) {
			page = 1;
		}
		if (rows == null) {
			rows = 10;
		}
		Map<String, Object> jsonMap = new HashMap<String, Object>();
		
		Map<String, Object> map = new HashMap<String, Object>();

		if (qureyString == null || qureyString.equals("")) {
			qureyString = null;
		}

		if (dateString == null || dateString.equals("")) {
			dateString = null;
		}

		map.put("qureyString", qureyString);
		map.put("dateString", dateString);
		map.put("pageSize", rows);
		map.put("offSet", (page - 1) * rows);

		List<Message> messageList = messageMapper.showMessageByQureyItems(map);

		jsonMap.put("total", messageMapper.getMessageByQureyItemsCount(map));
		jsonMap.put("rows", messageList);
		
		return jsonMap;
	}

	@Override
	public Map<String, Object> pushMsg(String message, String pushTag,
			String deviceType, HttpSession session) {
		Map<String, Object> jsonMap = new HashMap<String, Object>();
		boolean p_r = false;

		String username = (String) session.getAttribute("username");
		if (username == null || username.equals("")) {
			p_r = false;
		} else {
			if (deviceType != null && deviceType.equals("android")) {
				BaiduPushManager bpm = new BaiduPushManager();
				if (message != null && !pushTag.equals(null)) {
					p_r = bpm.pushMessageByTag(pushTag, message);
				}
			} else if (deviceType != null && deviceType.equals("ios")) {
				p_r = true;
			}

			Message record = new Message();
			record.setContent(message);
			record.setSenderName(username);
			record.setSendTime(new Date());
			record.setTag(1);

			messageMapper.insert(record);

			Long messageId = record.getId();

			int type = -1;
			if (pushTag.equals("teacher")) {
				type = 2;
			} else if (pushTag.equals("student")) {
				type = 3;
			}

			List<Long> receiverIds = userMapper.getUserIdByType(type);
			for (Long receiverId : receiverIds) {
				Receiver receiver = new Receiver();
				receiver.setMessageId(messageId);
				receiver.setReceiverId(receiverId);
				receiver.setStatus(0);
				receiver.setTag(1);
				receiverMapper.insert(receiver);
			}
		}

		jsonMap.put("result", p_r);

		return jsonMap;
	}
}
