package msgcenter.service;


import java.util.List;

import msgcenter.model.News;

public interface NewsServiceI {
	int deleteByPrimaryKey(Long id);

    int insert(News record);

    int insertSelective(News record);

    News selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(News record);

    int updateByPrimaryKey(News record);
    
    List<News> selectAllNews();

	int insertNews(News news);
}
