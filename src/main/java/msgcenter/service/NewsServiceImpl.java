package msgcenter.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import msgcenter.dao.NewsMapper;
import msgcenter.model.News;

@Service("newsService")
public class NewsServiceImpl implements NewsServiceI {

	private NewsMapper newsMapper;
	
	public NewsMapper getNewsMapper() {
		return newsMapper;
	}

	@Autowired
	public void setNewsMapper(NewsMapper newsMapper) {
		this.newsMapper = newsMapper;
	}

	public int deleteByPrimaryKey(Long id) {
		// TODO Auto-generated method stub
		return newsMapper.deleteByPrimaryKey(id);		
	}

	public int insert(News record) {
		// TODO Auto-generated method stub
		return newsMapper.insert(record);
	}

	public int insertSelective(News record) {
		// TODO Auto-generated method stub		
		return newsMapper.insertSelective(record);
	}

	public News selectByPrimaryKey(Long id) {
		// TODO Auto-generated method stub
		return newsMapper.selectByPrimaryKey(id);
	}

	public int updateByPrimaryKeySelective(News record) {
		// TODO Auto-generated method stub
		return newsMapper.updateByPrimaryKeySelective(record);
	}

	public int updateByPrimaryKey(News record) {
		// TODO Auto-generated method stub
		return newsMapper.updateByPrimaryKey(record);
	}

	public List<News> selectAllNews() {		
		return newsMapper.selectAllNews();
	}

	public int insertNews(News news) {
		
		return newsMapper.insertSelective(news);
	}

}
