package msgcenter.service;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.ui.Model;
import org.springframework.web.bind.support.SessionStatus;

import msgcenter.model.User;

public interface UserServiceI {

	public User getUserById(Long id);//获得用户信息
	
	public User checkUserLogin(String username,String password);//登录验证
	
	public int updateUserInfo(Long channelid,String userid,String username);
	
	public List<User> showAllUser();

	public Map<String, Object> checkUserLogin(String username, String password,
			Long channelid, String userid, Model model,
			SessionStatus sessionStatus, HttpSession session);

}
