package msgcenter.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import msgcenter.dao.UserMapper;
import msgcenter.model.User;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import org.springframework.web.bind.support.SessionStatus;

@Service("userService")
public class UserServiceImpl implements UserServiceI {

	private UserMapper userMapper;

	public UserMapper getUserMapper() {
		return userMapper;
	}

	@Autowired
	public void setUserMapper(UserMapper userMapper) {
		this.userMapper = userMapper;
	}

	public User getUserById(Long id) {
		return userMapper.selectByPrimaryKey(id);
	}

	public User checkUserLogin(String username, String password) {
		// TODO Auto-generated method stub
		HashMap<String, String> map = new HashMap<String, String>();
		map.put("username", username);
		map.put("password", password);
		return userMapper.checkUserLogin(map);
	}

	public int updateUserInfo(Long channelid, String userid, String username) {
		// TODO Auto-generated method stub
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("channelId", channelid);
		map.put("userId", userid);
		map.put("username", username);
		return userMapper.updateUserInfoByUsername(map);
	}

	public List<User> showAllUser() {
		return userMapper.showAllUser();

	}

	@Override
	public Map<String, Object> checkUserLogin(String username, String password,
			Long channelid, String userid, Model model,
			SessionStatus sessionStatus, HttpSession session) {
		session.removeAttribute("username");

		Map<String, Object> jsonMap = new HashMap<String, Object>();
		User user = null;
		List<Long> list = null;// groupId结果集，用于作为群组推送的tag
		boolean re = false;
		boolean b_re = false;
		if (!username.equals(null) && !password.equals(null)) {
			if ((user = checkUserLogin(username, password)) != null) {
				model.addAttribute("username", username);
				re = true;
				int role = user.getType();
				jsonMap.put("role", role);
				if (role == 2) {
					jsonMap.put("push_tag", "teacher");
				} else if (role == 3) {
					jsonMap.put("push_tag", "student");
				} else {
					jsonMap.put("push_tag", null);
				}
				/*
				 * 根据userid获得groupid作为pushTag Long userId=user.getId();
				 * list=groupMemberService.getGroupIdByUserId(userId);
				 * StringBuffer pushTag=new StringBuffer(); for (int i = 0; i <
				 * list.size(); i++) { pushTag.append(list.get(i).toString());
				 * if (i < list.size()-1) { pushTag.append(","); } }
				 * jsonMap.put("push_tag", pushTag.toString());
				 */
			}
		}
		jsonMap.put("result", re);
		if (re) {
			if (channelid != null && !userid.equals(null)) {
				if (updateUserInfo(channelid, userid, username) == 1)
					b_re = true;
			}
		}
		jsonMap.put("bind_result", b_re);
		return jsonMap;
	}

}
