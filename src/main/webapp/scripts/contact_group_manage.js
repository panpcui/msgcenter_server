$.ajax({
		url : "/msgcenter/userController/getSessionUsername.do",
		contentType : "application/x-www-form-urlencoded; charset=utf-8",
		success : function(msg) {
			if (msg=="") 
			{
				window.location.href="../../ login.html";
			}
		}
	});

function contact_group_add()
{
	$("#contact-group-id").hide();
	$("#belonger_id").val("");
	$("#name").val("");
	$("#tag").val("");
	$('#contact-group-edit-window').window('open');
}

function contact_group_del()
{
	var rows = $('#contact-group-main-datagrid').datagrid('getSelections');
	if(rows.length<1)
	{
		alert("至少选中一行！");
	}
	else
	{
		$.messager.confirm(
			'删除',
			'您确认想要删除记录吗？',
			function(r) {
				if (r) {
					for(var i=0;i<rows.length;i++)
					{
						$.ajax({
							url :"====================================?id="+rows[i].id,
							dataType : "json",
							contentType : "application/x-www-form-urlencoded; charset=utf-8",
							success : function(msg) {
								if (msg.result) {
									$.messager.show({
										title : '删除',
										msg : '删除成功！',
										timeout : 2000,
										showType : 'show',
									});
									$('#contact-group-main-datagrid').datagrid('reload');
								} else {
									alert("删除失败！");
								}
							}
						});
					}
				}
			});
	}
}

function contact_group_edit()
{
	var rows=$('#contact-group-main-datagrid').datagrid('getSelections');
	
	if(rows.length>1)
	{
		alert("只能进行单行修改！");
	}
	else if (rows.length <1) {
		alert("请选中一行");
	} else {
		$("#contact-group-id").show();
		$("#id").val(rows[0].id);
		$("#id").attr("readonly", true);
		$("#belonger_id").val(rows[0].belonger_id);
		$("#name").val(rows[0].name);
		$("#tag").val(rows[0].tag);
		$('#contact-group-edit-window').window('open');
	}
}

function contact_group_search() {
	var belongerId=$('#contact-group-belongerId').val();
	$.ajax({
		url : "===========================================?qureyString="
				+ belongerId,
		dataType : "json",
		contentType : "application/x-www-form-urlencoded; charset=utf-8",
		success : function(data) {
			$('#contact-group-main-datagrid').datagrid('loadData',data);
		}
	});
}

function contact_group_submit() {
	var belongerId=$("#belonger_id").val();
	var name=$("#name").val();
	var tag=$("#tag").val();
	if (belongerId == ""||name==""||tag=="") {
		alert("请查看信息是否填写完整！");
	} else {
		var id=$("#id").val();

		var r = confirm("确认添加？");
		if (r == true) {
				$.ajax({
					url : "===============================================================",
					dataType : "json",
					contentType : "application/x-www-form-urlencoded; charset=utf-8",
					success : function(msg) {
						if (msg.result) 
						{
							alert("添加成功！");
						} else 
						{
							alert("添加失败！");
						}
						$('#contact-group-edit-window').window('close');
						$('#contact-group-main-datagrid').datagrid('reload');
					}
				});
		}
	}
}

function contact_group_cancel() {
	$('#contact-group-edit-window').window('close');
}