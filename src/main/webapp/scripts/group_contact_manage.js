$.ajax({
		url : "/msgcenter/userController/getSessionUsername.do",
		contentType : "application/x-www-form-urlencoded; charset=utf-8",
		success : function(msg) {
			if (msg=="") 
			{
				window.location.href="../../ login.html";
			}
		}
	});

function group_contact_add()
{
	$("#group_id").val("");
	$("#user_id").val("");
	$("#tag").val("");
	$('#group-contact-edit-window').window('open');
}

function group_contact_del()
{
	var rows = $('#group-contact-main-datagrid').datagrid('getSelections');
	if(rows.length<1)
	{
		alert("至少选中一行！");
	}
	else
	{
		$.messager.confirm(
			'删除',
			'您确认想要删除记录吗？',
			function(r) {
				if (r) {
					for(var i=0;i<rows.length;i++)
					{
						$.ajax({
							url :"====================================?id="+rows[i].id,
							dataType : "json",
							contentType : "application/x-www-form-urlencoded; charset=utf-8",
							success : function(msg) {
								if (msg.result) {
									$.messager.show({
										title : '删除',
										msg : '删除成功！',
										timeout : 2000,
										showType : 'show',
									});
									$('#group-contact-main-datagrid').datagrid('reload');
								} else {
									alert("删除失败！");
								}
							}
						});
					}
				}
			});
	}
}

function group_contact_edit()
{
	var rows=$('#group-contact-main-datagrid').datagrid('getSelections');
	
	if(rows.length>1)
	{
		alert("只能进行单行修改！");
	}
	else if (rows.length <1) {
		alert("请选中一行");
	} else {
		$("#group_id").val(rows[0].group_id);
		$("#user_id").val(rows[0].user_id);
		$("#tag").val(rows[0].tag);
		$('#group-contact-edit-window').window('open');
	}
}

function group_contact_search() {
	var groupId=$('#group-contact-groupId').val();
	$.ajax({
		url : "===========================================?qureyString="
				+ groupId,
		dataType : "json",
		contentType : "application/x-www-form-urlencoded; charset=utf-8",
		success : function(data) {
			$('#group-contact-main-datagrid').datagrid('loadData',data);
		}
	});
}

function group_contact_submit() {
	var groupId=$("#group_id").val();
	var userId=$("#user_id").val();
	var tag=$("#tag").val();
	if (groupId == ""||userId==""||tag=="") {
		alert("请查看信息是否填写完整！");
	} else {
		var r = confirm("确认添加？");
		if (r == true) {
				$.ajax({
					url : "===============================================================",
					dataType : "json",
					contentType : "application/x-www-form-urlencoded; charset=utf-8",
					success : function(msg) {
						if (msg.result) 
						{
							alert("添加成功！");
						} else 
						{
							alert("添加失败！");
						}
						$('#group-contact-edit-window').window('close');
						$('#group-contact-main-datagrid').datagrid('reload');
					}
				});
		}
	}
}

function group_contact_cancel() {
	$('#group-contact-edit-window').window('close');
}