$.ajax({
		url : "/msgcenter/userController/getSessionUsername.do",
		contentType : "application/x-www-form-urlencoded; charset=utf-8",
		success : function(msg) {
			if (msg=="") 
			{
				window.location.href="../../login.html";
			}
		}
	});

$("#main-tree").tree({
	onClick : function(node) {
		//alert(node.id);
		if($("#main-tree").tree('isLeaf',node.target))
		{
			if($('#main-tabs').tabs('getTab',node.text)==null){				
				$('#main-tabs').tabs('add', {
					id : node.id,
					title : node.text,
					href : node.id + ".html",
					closable : true,
					});
			}else{
				$('#main-tabs').tabs("select",node.text);
			}
			
		}

	}
});

function exit()
{
	$.ajax({
		url : "/msgcenter/userController/clearSessionUsername.do",
		contentType : "application/x-www-form-urlencoded; charset=utf-8",
		success : function() {
				window.location.href="../../login.html";
		}
	});
}