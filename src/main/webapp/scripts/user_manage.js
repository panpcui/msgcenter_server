$.ajax({
		url : "/msgcenter/userController/getSessionUsername.do",
		contentType : "application/x-www-form-urlencoded; charset=utf-8",
		success : function(msg) {
			if (msg=="") 
			{
				window.location.href="../../ login.html";
			}
		}
	});

function user_add()
{
	$("#user-id").hide();
	$("#username").val("");
	$("#password").val("");
	$("#name").val("");
	$("#tag").val("");
	$('#user-edit-window').window('open');
}

function user_del()
{
	var rows = $('#user-main-datagrid').datagrid('getSelections');
	if(rows.length<1)
	{
		alert("至少选中一行！");
	}
	else
	{
		$.messager.confirm(
			'删除',
			'您确认想要删除记录吗？',
			function(r) {
				if (r) {
					for(var i=0;i<rows.length;i++)
					{
						$.ajax({
							url :"====================================?id="+rows[i].id,
							dataType : "json",
							contentType : "application/x-www-form-urlencoded; charset=utf-8",
							success : function(msg) {
								if (msg.result) {
									$.messager.show({
										title : '删除',
										msg : '删除成功！',
										timeout : 2000,
										showType : 'show',
									});
									$('#user-main-datagrid').datagrid('reload');
								} else {
									alert("删除失败！");
								}
							}
						});
					}
				}
			});
	}
}

function user_edit()
{
	var rows=$('#user-main-datagrid').datagrid('getSelections');
	
	if(rows.length>1)
	{
		alert("只能进行单行修改！");
	}
	else if (rows.length <1) {
		alert("请选中一行");
	} else {
		$("#user-id").show();
		$("#id").val(rows[0].id);
		$("#id").attr("readonly", true);
		$("#username").val(rows[0].username);
		$("#password").val(rows[0].password);
		$("#name").val(rows[0].name);
		$("#tag").val(rows[0].tag);
		$('#user-edit-window').window('open');
	}
}

function user_search() {
	var userType=$('#user-type').combobox('getValue');
	$.ajax({
		url : "===========================================?qureyString="
				+ userType,
		dataType : "json",
		contentType : "application/x-www-form-urlencoded; charset=utf-8",
		success : function(data) {
			$('#user-main-datagrid').datagrid('loadData',data);
		}
	});
}

function user_submit() {
	var username=$("#username").val();
	var password=$("#password").val();
	var name=$("#name").val();
	var tag=$("#tag").val();
	if (username == ""||password==""||name==""||tag=="") {
		alert("请查看信息是否填写完整！");
	} else {
		var id=$("#id").val();
		var type=$("#type").combobox("getValue");

		var r = confirm("确认添加？");
		if (r == true) {
				$.ajax({
					url : "===============================================================",
					dataType : "json",
					contentType : "application/x-www-form-urlencoded; charset=utf-8",
					success : function(msg) {
						if (msg.result) 
						{
							alert("添加成功！");
						} else 
						{
							alert("添加失败！");
						}
						$('#user-edit-window').window('close');
						$('#user-main-datagrid').datagrid('reload');
					}
				});
		}
	}
}

function user_cancel() {
	$('#user-edit-window').window('close');
}