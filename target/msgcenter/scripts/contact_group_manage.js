function add() {

	$('#contact-group-edit-window').window({
		href : 'contact_group_edit.html'
	});

	$('#contact-group-edit-window').window('open');

}

function del() {

	var row = $('#contact-group-datagrid').datagrid('getSelected');
	var selectIndex = $('#contact-group-datagrid').datagrid('getRowIndex', row);
	if (row == null) {
		alert("请选中一行");
	} else {
		$.messager.confirm('删除', '您确认想要删除记录吗？', function(r) {
			if (r) {
				$('#contact-group-datagrid').datagrid('deleteRow', selectIndex);
				$.messager.show({
					title : '删除',
					msg : 'ok',
					timeout : 2000,
					showType : 'show',
					style : {
						right : '',
						top : document.body.scrollTop
								+ document.documentElement.scrollTop,
						bottom : ''
					}
				});
			}
		});
	}

}

function edit() {
	var row = $('#contact-group-datagrid').datagrid('getSelected');

	if (row == null) {
		alert("请选中一行");
	} else {
		$('#contact-group-edit-window').window({
			href : 'contact_group_edit.html',
		});

		$('#contact-group-edit-window').window('open');

	}
}