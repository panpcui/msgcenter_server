function add() {

	$('#group-contact-edit-window').window({
		href : 'group_contact_edit.html'
	});

	$('#group-contact-edit-window').window('open');

}

function del() {

	var row = $('#group-contact-datagrid').datagrid('getSelected');
	var selectIndex = $('#group-contact-datagrid').datagrid('getRowIndex', row);
	if (row == null) {
		alert("请选中一行");
	} else {
		$.messager.confirm('删除', '您确认想要删除记录吗？', function(r) {
			if (r) {
				$('#group-contact-datagrid').datagrid('deleteRow', selectIndex);
				$.messager.show({
					title : '删除',
					msg : 'ok',
					timeout : 2000,
					showType : 'show',
					style : {
						right : '',
						top : document.body.scrollTop
								+ document.documentElement.scrollTop,
						bottom : ''
					}
				});
			}
		});
	}

}

function edit() {
	var row = $('#group-contact-datagrid').datagrid('getSelected');

	if (row == null) {
		alert("请选中一行");
	} else {
		$('#group-contact-edit-window').window({
			href : 'group_contact_edit.html',
		});

		$('#group-contact-edit-window').window('open');

	}
}