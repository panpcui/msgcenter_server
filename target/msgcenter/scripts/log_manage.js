function add() {

	$('#log-edit-window').window({
		href : 'log_edit.html'
	});

	$('#log-edit-window').window('open');

}

function del() {

	var row = $('#log-datagrid').datagrid('getSelected');
	var selectIndex = $('#log-datagrid').datagrid('getRowIndex', row);
	if (row == null) {
		alert("请选中一行");
	} else {
		$.messager.confirm('删除', '您确认想要删除记录吗？', function(r) {
			if (r) {
				$('#user-datagrid').datagrid('deleteRow', selectIndex);
				$.messager.show({
					title : '删除',
					msg : 'ok',
					timeout : 2000,
					showType : 'show',
					style : {
						right : '',
						top : document.body.scrollTop
								+ document.documentElement.scrollTop,
						bottom : ''
					}
				});
			}
		});
	}

}

function edit() {
	var row = $('#log-datagrid').datagrid('getSelected');

	if (row == null) {
		alert("请选中一行");
	} else {
		$('#log-edit-window').window({
			href : 'log_edit.html',
		});

		$('#log-edit-window').window('open');

	}
}