$.ajax({
		url : "/msgcenter/userController/getSessionUsername.do",
		contentType : "application/x-www-form-urlencoded; charset=utf-8",
		success : function(msg) {
			if (msg=="") 
			{
				window.location.href="../../ login.html";
			}
		}
	});

$.fn.datebox.defaults.formatter = function(date) {
	var y = date.getFullYear();
	var m = date.getMonth() + 1;
	var d = date.getDate();
	if (m < 10) {
		if (d < 10)
			return y + '-0' + m + '-0' + d;
		else
			return y + '-0' + m + '-' + d;
	} else {
		if (d < 10)
			return y + '-0' + m + '-0' + d;
		else
			return y + '-0' + m + '-' + d;
	}
}

function message_add() {
	$('#message-edit-window').window('open');
}

/*function message_del() {
	var rows = $('#message-main-datagrid').datagrid('getSelections');
	if(rows.length<1)
	{
		alert("至少选中一行！");
	}
	else
	{
		$.messager.confirm(
			'删除',
			'您确认想要删除记录吗？',
			function(r) {
				if (r) {
					for(var i=0;i<rows.length;i++)
					{
						var selectId = rows[i].id;
						$.ajax({
							url : "/msgcenter/messageController/deleteMessage.do?id="
									+ selectId,
							dataType : "json",
							contentType : "application/x-www-form-urlencoded; charset=utf-8",
							success : function(msg) {
								if (msg.result) {
									$.messager.show({
										title : '删除',
										msg : '删除成功！',
										timeout : 2000,
										showType : 'show',

									});
									$('#message-main-datagrid').datagrid('reload');
								} else {
									alert("删除失败！");
								}
							}
						});
					}
				}
			});
	}
}*/

function message_search() {
	var keyword=$('#search-keyword').val();
	var date=$('#search-time').datebox('getValue');
	$.ajax({
		url : "/msgcenter/messageController/showMessageByQureyItems.do?qureyString="
				+ keyword
				+"&dateString="
				+date,
		dataType : "json",
		contentType : "application/x-www-form-urlencoded; charset=utf-8",
		success : function(data) {
			$('#message-main-datagrid').datagrid('loadData',data);
		}
	});

}

var ppl = 60;
var maxl = 200;
function message_show() {
	var s = document.getElementById("textarea").value.length + 1;
	if (s > maxl)
		document.getElementById("textarea").value = document
				.getElementById("textarea").value.substr(0, maxl - 1);
	else
		document.getElementById("limit").innerHTML = "已输入：" + s + "/" + maxl
				+ " 字符";
}

function message_submit() {
	var message = $("#textarea").val();
	if (message == "") {
		alert("消息内容不能为空！");
	} else {
		var id=$("#id").val();
		var role = document.getElementById("role");
		var pushTag = role.options[role.selectedIndex].value;

		var phone = document.getElementsByName("phone");
		var deviceType = "ios";

		for ( var i = 0; i < phone.length; i++) {
			if (phone.item(i).checked) {
				deviceType = phone.item(i).getAttribute("value");
				break;
			}
		}
		
		var r = confirm("确认发送？");
		if (r == true) {
				$.ajax({
					url : "/msgcenter/messageController/pushMessage.do?message="
							+ message
							+ "&pushTag="
							+ pushTag
							+ "&deviceType="
							+ deviceType,
					dataType : "json",
					contentType : "application/x-www-form-urlencoded; charset=utf-8",
					success : function(msg) {
						if (msg.result) 
						{
							alert("推送成功！");
						} else 
						{
							alert("推送失败！请重新推送");
						}
						$('#message-edit-window').window('close');
						$('#message-main-datagrid').datagrid('reload');
					}
				});
		}
	}
}

function message_cancel() {
	$('#message-edit-window').window('close');
}
