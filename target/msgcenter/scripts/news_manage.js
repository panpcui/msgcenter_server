$.ajax({
		url : "/msgcenter/userController/getSessionUsername.do",
		contentType : "application/x-www-form-urlencoded; charset=utf-8",
		success : function(msg) {
			if (msg=="") 
			{
				window.location.href="../../ login.html";
			}
		}
	});

function news_add()
{
	$("#news-id").hide();
	$("#title").val("");
	$("#content").val("");
	$("#fileUrl").val("");
	$("#imgUrl").val("");
	$('#news-edit-window').window('open');
}

function news_del()
{
	var rows = $('#news-main-datagrid').datagrid('getSelections');
	if(rows.length<1)
	{
		alert("至少选中一行！");
	}
	else
	{
		$.messager.confirm(
			'删除',
			'您确认想要删除记录吗？',
			function(r) {
				if (r) {
					for(var i=0;i<rows.length;i++)
					{
						$.ajax({
							url :"/msgcenter/newsController/deleteNews.do?id="+rows[i].id,
							dataType : "json",
							contentType : "application/x-www-form-urlencoded; charset=utf-8",
							success : function(msg) {
								if (msg.result) {
									$.messager.show({
										title : '删除',
										msg : '删除成功！',
										timeout : 2000,
										showType : 'show',
									});
									$('#news-main-datagrid').datagrid('reload');
									location.reload();
								} else {
									alert("删除失败！");
								}
							}
						});
					}
				}
			});
	}
}

function news_edit()
{
	var rows=$('#news-main-datagrid').datagrid('getSelections');
	
	if(rows.length>1)
	{
		alert("只能进行单行修改！");
	}
	else if (rows.length <1) {
		alert("请选中一行");
	} else {
		//$("#news-id").show();
		//$("#news-id").val(rows[0].id);
		$("#title").val(rows[0].title);
		$("#content").val(rows[0].content);
		$("#fileUrl").val(rows[0].fileUrl);
		$("#imgUrl").val(rows[0].imgUrl);
		$('#news-edit-window').window('open');
	}
}

function news_submit()
{
	var title=$("#title").val();
	var content=$("#content").val();
	var fileurl=$("#fileUrl").val();
	var imgurl=$("#imgUrl").val();
	if(title==""||content==""||fileurl==""||imgurl=="")
	{
		alert("请查看是否填写完整！");
	}
	else
	{
		var r=confirm("确认发布？");
		if(r==true)
		{
			document.getElementById("news-edit-form").submit();
			$("#news-edit-window").window('close');
			$('#news-main-datagrid').datagrid('reload'); 
		}
	}
}

function news_cancel()
{
	$('#news-edit-window').window('close');
}

