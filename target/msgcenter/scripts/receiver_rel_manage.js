function add() {

	$('#receiver-rel-edit-window').window({
		href : 'receiver_rel_edit.html'
	});

	$('#receiver-rel-edit-window').window('open');

}

function del() {

	var row = $('#receiver-rel-datagrid').datagrid('getSelected');
	var selectIndex = $('#receiver-rel-datagrid').datagrid('getRowIndex', row);
	if (row == null) {
		alert("请选中一行");
	} else {
		$.messager.confirm('删除', '您确认想要删除记录吗？', function(r) {
			if (r) {
				$('#receiver-rel-datagrid').datagrid('deleteRow', selectIndex);
				$.messager.show({
					title : '删除',
					msg : 'ok',
					timeout : 2000,
					showType : 'show',
					style : {
						right : '',
						top : document.body.scrollTop
								+ document.documentElement.scrollTop,
						bottom : ''
					}
				});
			}
		});
	}

}

function edit() {
	var row = $('#receiver-rel-datagrid').datagrid('getSelected');

	if (row == null) {
		alert("请选中一行");
	} else {
		$('#receiver-rel-edit-window').window({
			href : 'receiver-rel_edit.html',
		});

		$('#receiver-rel-edit-window').window('open');

	}
}